// tag::baseClass[]
//tag::orderForm[]
package com.example.tacocloud.controller;
//end::orderForm[]

import com.example.tacocloud.TacoOrder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;


@Slf4j
@Controller
@RequestMapping("/orders")
public class OrderController {

//end::baseClass[]
  @GetMapping("/current")
  public String orderForm(Model model) {
    model.addAttribute("tacoOrder", new TacoOrder());
    return "orderForm";
  }
//end::orderForm[]

/*
//tag::handlePost[]
  @PostMapping
  public String processOrder(Order order) {
    log.info("Order submitted: " + order);
    return "redirect:/";
  }
//end::handlePost[]
*/

//tag::handlePostWithValidation[]
  @PostMapping
  public String processOrder(@Valid TacoOrder order, Errors errors) {
    if (errors.hasErrors()) {
      return "orderForm";
    }
    log.info("Order submitted: " + order);
    return "redirect:/";
  }
//  public String processOrder(TacoOrder order, Errors errors) {
//    if (errors.hasErrors()) {
//      return "orderForm";
//    }
//
//    log.info("Order submitted: " + order);
//    return "redirect:/";
//  }
//end::handlePostWithValidation[]

//tag::baseClass[]
//tag::orderForm[]

}
//end::orderForm[]
//end::baseClass[]
